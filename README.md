FLIGHT SEARCH (ZF2)
===================

Installation using Composer
---------------------------

1. git clone https://flolopdel@bitbucket.org/flolopdel/zend.git /path/to/project

2. composer update into project folder

3. php -S 0.0.0.0:8080 -t public/ public/index.php

4. http://0.0.0.0:8080/flights
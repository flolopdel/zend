


$(document).ready(function() {


	var _weekday = {
		"0": "Sun",
		"1": "Mon",
		"2": "Tue",
		"3": "Wed",
		"4": "Thu",
		"5": "Fri",
		"6": "Sat"
	};

	var _datepickerOptions = { format: 'YYYY-MM-DD'};
	var enabledDates = [];
	_datepickerOptions.enabledDates = enabledDates;
	$( "#departure_date" ).datetimepicker(_datepickerOptions);
	$( "#return_date" ).datetimepicker(_datepickerOptions);

	$('#from_select').on('change', function() {
	    refreshForm();
	});

	$('#to_select').on('change', function() {
	    refreshForm();
	});

	$('#out_table tr').on('click', function() {
	    $(this).find('td input:radio').prop('checked', true);
	    refreshDetails();
	});

	$('#ret_table tr').on('click', function() {
	    $(this).find('td input:radio').prop('checked', true);
	    refreshDetails();
	});

	$('.ret_radio').on('click', function() {
	    refreshDetails();
	});

	$('.out_radio').on('click', function() {
	    refreshDetails();
	});

	$('.roundtrip').on('click', function() {
	    customForm(this);
	});

	function customForm(radio){
		//TODO

		if(parseInt($(radio).val())) {
			$( "#return_date" ).prop('disabled', false);
		}else{
			$( "#return_date" ).prop('disabled', true);
		}
		
	}

	function refreshDetails() {
		//refresh partners, ms and countries, fill importations
		var outSelected 		= $(".out_radio:checked");
		var retSelected 		= $(".ret_radio:checked");
		var indexOut 			= outSelected.val();
		var indexRet 			= retSelected.val();
		var outDetails, retDetails;

		if((!_roundtrip && indexOut) || (indexOut && indexRet)){
			var outDetails 	= _outsbound[indexOut];
			outDetails.dateFormat = parseDateFormat(outDetails.datetime);
			outDetails.timeFormat = parseTimeFormat(outDetails.datetime);

			$(".details_flight").css('display','table');
			$(".details_flight_ret").css('display','none');

			$(".details_flight_out .d_date").html(outDetails.dateFormat);
			$(".details_flight_out .d_code").html('- '+outDetails.aircrafttype);
			$(".details_flight_out .d_departure").html(outDetails.depart.airport.name);
			$(".details_flight_out .d_destination").html(outDetails.arrival.airport.name);
			$(".details_flight_out .d_time").html(outDetails.timeFormat);
			$(".details_flight_out .d_price").html('€ '+outDetails.price);
			$(".details_flight_passenger .d_passengers").html(_passengers);

			if(_roundtrip){
				var retDetails 	= _return[indexRet];

				retDetails.dateFormat = parseDateFormat(retDetails.datetime);
				retDetails.timeFormat = parseTimeFormat(retDetails.datetime);

				$(".details_flight_ret").css('display','table');

				$(".details_flight_ret .d_date").html(retDetails.dateFormat);
				$(".details_flight_ret .d_code").html('- '+retDetails.aircrafttype);
				$(".details_flight_ret .d_departure").html(retDetails.depart.airport.name);
				$(".details_flight_ret .d_destination").html(retDetails.arrival.airport.name);
				$(".details_flight_ret .d_price").html('€ '+retDetails.price);
				$(".details_flight_ret .d_time").html(retDetails.timeFormat);
			}
		}

	}

	function parseDateFormat(datetime){
		var response;
		var date 	= new Date(datetime);

		response = _weekday[date.getUTCDay()]+" "+(date.getUTCDate()<10?'0':'') +date.getUTCDate()+"/"+((date.getUTCMonth()+1)<10?'0':'') +(date.getUTCMonth()+1)+"/"+date.getUTCFullYear();

		return response;

	}

	function parseTimeFormat(datetime){
		var response;
		var date 	= new Date(datetime);

		response = (date.getUTCHours()<10?'0':'') +date.getUTCHours()+":"+(date.getUTCMinutes()<10?'0':'') +date.getUTCMinutes();

		return response;

	}

	function refreshForm() {
		//refresh partners, ms and countries, fill importations
		var fromCode 	= $("#from_select").val();
		var toCode 		= $("#to_select").val();

		if(toCode && fromCode){
			refreshDateAvaliable(fromCode, toCode);
		}else if (toCode && !fromCode){
			refreshDepartures(toCode);
		}else if (!toCode && fromCode){
			refreshDestinations(fromCode);
		}else{
			//Restart select
			//updateFlightRoutes({}, 'from_select');
			//updateFlightRoutes({}, 'to_select');
		}
	}

	function refreshDateAvaliable(fromCode, toCode){
		var params = {
			'destinationairport': toCode, 'departureairport': fromCode, 'returndepartureairport': toCode, 'returndestinationairport': fromCode
		};
		setAvailablesDate(params);

	}

	function refreshDepartures(toCode){
		var params = {
			'destinationairport': toCode
		};
		updateFlightRoutes(params, 'from_select');

	}

	function refreshDestinations(fromCode){
		var params = {
			'departureairport': fromCode
		};
		updateFlightRoutes(params, 'to_select');
	}

	function updateFlightRoutes(params, idElement){
		params.locale = 'nl_BE';
		var emptyLabel = (idElement == 'from_select')? 'From' : 'To';
		var keyCode = (idElement == 'from_select')? 'Dep' : 'Ret';

		$("#"+idElement).children("option").remove();
		$("#"+idElement).html('<option value>---</option>');

		$.ajax({
			type:'GET',
			url:'http://dezemerel.ddns.net/api/json/1F/flightroutes/',
			data: params,
			success: function(response) {
				var options ='<option value>'+emptyLabel+'</option>';
				//clear partner options
				$("#"+idElement).children("option").remove();
				for (value in response.flightroutes){
					options += '<option value="'+ response.flightroutes[value][keyCode+'Code']+'">'+ response.flightroutes[value][keyCode+'Name'] + '</option>'
				}
				$("#"+idElement).html(options);
			},
			timeout: 5000,
			error: function(xhr,err) {
				alert("AN ERROR HAS BEEN OCURRED\nreadyState: "+xhr.readyState+"\nstatus: "+xhr.status);
				alert("responseText: "+xhr.responseText);
			},
			dataType: 'json'
		});

	}

	function setAvailablesDate(params, idElement){
		params.locale = 'nl_BE';


		var _datepickerOptions = { format: 'YYYY-MM-DD'};

		$.ajax({
			type:'GET',
			url:'http://dezemerel.ddns.net/api/json/1F/flightschedules/',
			data: params,
			success: function(response) {
				var enabledDates = [];
				for (value in response.flightschedules.OUT){
					
					enabledDates.push(response.flightschedules.OUT[value]['date']);
				}

				_datepickerOptions.enabledDates = enabledDates;
				$('#departure_date').data("DateTimePicker").destroy();
				$( "#departure_date" ).datetimepicker(_datepickerOptions);


				var enabledDates = [];
				for (value in response.flightschedules.RET){
					
					enabledDates.push(response.flightschedules.RET[value]['date']);
				}
				_datepickerOptions.enabledDates = enabledDates;
				$('#return_date').data("DateTimePicker").destroy();
				$( "#return_date" ).datetimepicker(_datepickerOptions);

			},
			timeout: 5000,
			error: function(xhr,err) {
				alert("AN ERROR HAS BEEN OCURRED\nreadyState: "+xhr.readyState+"\nstatus: "+xhr.status);
				alert("responseText: "+xhr.responseText);
			},
			dataType: 'json'
		});

	}


		
});
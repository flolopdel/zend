<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Flight\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Flight\Model\Flight;
use Flight\Form\FlightForm;
use Flight\Form\SelectionForm;

use Flight\Service\JetairService;

class FlightController extends AbstractActionController
{
    public function indexAction()
    {
    	$form = new FlightForm();

		$requestForm = $this->getRequest();

		if ($requestForm->isPost()) { 
			//$flight = new Flight();
            $form->setInputFilter($form->getInputFilter());
            $form->setData($requestForm->getPost());
			if ($form->isValid()) { 
				//$flight->exchangeArray($form->getData());
                return $this->redirect()->toRoute('flights', array(
                    'action' => 'details',
                    'from' => $requestForm->getPost('from'),
                    'to' => $requestForm->getPost('to'),
                    'departure' => date('Ymd',strtotime($requestForm->getPost('departure'))),
                    'return' => date('Ymd',strtotime($requestForm->getPost('return'))),
                    'adult' => $requestForm->getPost('adult'),
                    'children' => $requestForm->getPost('children'),
                    'babies' => $requestForm->getPost('babies'),
                    'roundtrip' => $requestForm->getPost('roundtrip'),
                ));
			}
		}

		return array('form' => $form);
    }

    public function detailsAction()
    {
        $params = array();

        $params['departureairport'] = $this->params()->fromRoute('from');
        $params['destinationairport'] = $this->params()->fromRoute('to');
        $params['returndestinationairport'] = $this->params()->fromRoute('from');
        $params['returndepartureairport'] = $this->params()->fromRoute('to');
        $params['departuredate'] = $this->params()->fromRoute('departure');
        $params['returndate'] = $this->params()->fromRoute('return');
        $params['adults'] = $this->params()->fromRoute('adult');
        $params['children'] = $this->params()->fromRoute('children');
        $params['infants'] = $this->params()->fromRoute('babies');
        $params['outPagination'] = $this->params()->fromRoute('outPagination');
        $params['retPagination'] = $this->params()->fromRoute('retPagination');
        $params['roundtrip'] = $this->params()->fromRoute('roundtrip');

        if($params['outPagination']){
            if($params['outPagination'] == "later"){
                $params['departuredate'] = date('Ymd',strtotime($params['departuredate']. '+1 day'));
            }else if($params['outPagination'] == "earlier"){
                $params['departuredate'] = date('Ymd',strtotime($params['departuredate']. '-1 day'));
            }
        }

        if($params['retPagination']){
            if($params['retPagination'] == "later"){
                $params['returndate'] = date('Ymd',strtotime($params['returndate']. '+1 day'));
            }else if($params['retPagination'] == "earlier"){
                $params['returndate'] = date('Ymd',strtotime($params['returndate']. '-1 day'));
            }
        }
        $passengers = $params['adults']." adults";
        if($params['children']){
            $passengers .= ", ".$params['children']." children";
        }
        if($params['infants']){
            $passengers .= ", ".$params['infants']." babies";
        }

        if(!((int)$params['roundtrip'])){
            unset($params['returndestinationairport']);
            unset($params['returndepartureairport']);
            unset($params['returndate']);
        }

        $jetairClient = new JetairService();
        $flightDetails = $jetairClient->getFlightAvailability($params);

        $outsbound = array();

        if($flightDetails->flights->OUT){
            $outsbound = $flightDetails->flights->OUT;
        }


        $return = array();
        if((int)$params['roundtrip'] && $return = $flightDetails->flights->RET){
            $return = $flightDetails->flights->RET;
        }

        $form = new SelectionForm();
        
        $requestForm = $this->getRequest();

        if ($requestForm->isPost()) { 
            $form->setInputFilter($form->getInputFilter());
            $form->setData($requestForm->getPost());

            if ($form->isValid()) {
                $formData           = $form->getData(); 
                $outsbound_index    = (int)$formData['outselection'];
                $return_index       = (int)$formData['returnselection'];

                return $this->showAction($outsbound[$outsbound_index],$return[$return_index]);
            }
        }



        return array('form' => $form, 'outsbound' => $outsbound, 'return' => $return, 'passengers' => $passengers, 'roundtrip' => (int)$params['roundtrip']);

    }

    public function showAction($outsbound, $return)
    {
        var_dump($return);die;

        return array('outsbound' => $outsbound, 'return' => $return, 'action' => 'show');   
    }

    public function deleteAction()
    {

    }
}

<?php

namespace Flight\Form;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

use Flight\Service\JetairService;

class FlightForm extends Form {

	protected $inputFilter;

	public function __construct($name = null) {
        // we want to ignore the name passed
		parent::__construct('flight_search');

        $jetairClient = new JetairService();

        $departures = $jetairClient->getValueSelectDepartures();
        $destinations = $jetairClient->getValueSelectDestinations();
		
		$this->add(array(
			'name' => 'roundtrip', 
			'type' => 'Radio',
			'options' => array(
				'value_options' => array(
					1 => 'Round-trip flight',
					0 => 'One way',
				),
			),
			'attributes' => array(
				'class' => 'roundtrip',
			),
		));
		$this->add(array(
			'name' => 'from', 
			'type' => 'Select',
			'options' => array(
                'value_options' => $departures,
                'empty_option' => 'From',
			),
			'attributes' => array(
				'class' => 'form-control input-md',
				'id' => 'from_select',
				'placeholder' => 'From',
			),
		)); 
		$this->add(array(
			'name' => 'to',
			'type' => 'Select',
			'options' => array(
                'value_options' => $destinations,
                'empty_option' => 'To',
			),
			'attributes' => array(
				'class' => 'form-control input-md',
				'placeholder' => 'To',
				'id' => 'to_select',
			),
		));
		$this->add(array(
			'name' => 'departure',
			'type' => 'text',
			'options' => array(
				'format' => 'Y-m-d'
			),
			'attributes' => array(
				'class' => 'form-control input-md',
				'placeholder' => 'Departure',
				'min' => date('Y-m-d'),
				'max' => '2020-01-01',
				'step' => '1', // days; default step interval is 1 day
				'id' => 'departure_date',
			),
		));
		$this->add(array(
			'name' => 'return',
			'type' => 'text',
			'options' => array(
				'format' => 'Y-m-d'
			),
			'attributes' => array(
				'class' => 'form-control input-md',
				'placeholder' => 'Return',
				'min' => date('Y-m-d'),
				'max' => '2020-01-01',
				'step' => '1', // days; default step interval is 1 day
				'id' => 'return_date',
			),
		));
		$this->add(array(
			'name' => 'adult',
			'type' => 'Select',
			'options' => array(
                'value_options' => $this->getDefaultSelectValue(9, 'Adults', 1),
			),
			'attributes' => array(
				'class' => 'form-control input-md',
				'placeholder' => 'Adult',
				'value' => '1',
			),
		));
		$this->add(array(
			'name' => 'children',
			'type' => 'Select',
			'options' => array(
                'value_options' => $this->getDefaultSelectValue(9, 'Children'),
            ),
			'attributes' => array(
				'class' => 'form-control input-md',
				'placeholder' => 'Children',
			),
		));
		$this->add(array(
			'name' => 'babies',
			'type' => 'Select',
			'options' => array(
                'value_options' => $this->getDefaultSelectValue(2, 'Babies'),
            ),
			'attributes' => array(
				'class' => 'form-control input-md',
				'placeholder' => 'Babies',
			),
		));
		$this->add(array(
			'name' => 'submit',
			'type' => 'Submit',
			'attributes' => array(
			    'value' => 'Go',
			    'id' => 'submitbutton',
				'class' => 'form-control input-md btn btn-success',
			),
		)); 
	}

	public function getInputFilter() {
		if (!$this->inputFilter) { 
			$inputFilter = new InputFilter();

			$inputFilter->add(array( 
				'name' => 'from', 
				'required' => true,
			));

			$inputFilter->add(array( 
				'name' => 'to', 
				'required' => true, 
			));

			$inputFilter->add(array( 
				'name' => 'departure', 
				'required' => true,
		        'validators' => array(
		            array(
		                'name' => 'not_empty',
		            ),
		        ),
			));

			$inputFilter->add(array( 
				'name' => 'adult', 
				'required' => true,
		        'validators' => array(
		            array(
		                'name' => 'not_empty',
		            ),
		        ),
			));

            $this->inputFilter = $inputFilter;
        }
		return $this->inputFilter; 
	}

	private function getDefaultSelectValue($value, $label, $initValue = 0){

		$result = array();

		for ($i = $initValue; $i < $value; $i++) {
			$result[$i] = $i . ' '. $label;
			
		}

		return $result;

	}
}